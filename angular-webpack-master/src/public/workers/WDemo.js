importScripts('./WorkerFactory.js');

onmessage = function (message) {
  // console.log('url',message.data.url);
  // console.log('data',message.data.json);
  var xhr = new XHRProvider(message.data.url);
  xhr.async = true;
  xhr.method = 'POST';
  xhr.withCredentials = false;
  xhr.create(message.data.json);
  xhr.onreadystatechange(function (instance) {
    if (instance.readyState === XMLHttpRequest.DONE && instance.status === 200) {
      this.postMessage({'user': JSON.parse(instance.responseText), 'status':instance.status});
      close();
    }
  });
  xhr.run();
};
