'use strict';

const RESPONSE_TYPES = {
    'DOM': '',
    'JSON': 'json',
    'ARRAY_BUFFER': 'arraybuffer',
    'BLOB': 'blob',
    'DOCUMENT': 'document',
    'TEXT': 'text'
};

const METHOD = {
    'GET': 'GET',
    'HEAD': 'HEAD',
    'POST': 'POST',
    'PUT': 'PUT',
    'DELETE': 'DELETE',
    'OPTIONS': 'OPTIONS',
    'PATCH': 'PATCH',
    'TRACE': 'TRACE'
};

function XHRProvider(_url) {
    this.url = _url;
    this.method = METHOD.GET;
    this.async = true;
    this.responseType = RESPONSE_TYPES.JSON;
    this.withCredentials = false;
    this.data= null;
    this.xhr = null;

    this.create = function (data) {
        try {
            this.xhr = new XMLHttpRequest();
        } catch (e) {
            this.xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        this.data = data;
    };

    this.run = function () {
        this.xhr.open(this.method, this.url, this.async);
        this.xhr.send(this.data);
    };

    this.onreadystatechange = function (callback) {
        var self = this;
        this.xhr.onreadystatechange = function () {
            callback(self.xhr);
        };
    };
}
