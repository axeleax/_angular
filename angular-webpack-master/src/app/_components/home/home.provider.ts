import { StoreModule } from '@ngrx/store';
import { User } from './home.reducer';

export const HomeProvider = StoreModule.provideStore ( {
  User
} );
