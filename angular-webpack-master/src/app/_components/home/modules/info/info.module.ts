import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { InfoComponent } from './info.component';

@NgModule ( {
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    InfoComponent
  ],
  exports: [
    InfoComponent
  ],
  providers: []
} )
export class InfoModule {
}
