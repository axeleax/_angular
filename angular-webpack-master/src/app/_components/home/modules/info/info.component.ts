import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { EventsService } from '../../../../_services/events.service';

@Component ( {
  selector: 'my-info',
  templateUrl: './info.component.html',
  styleUrls: [ './info.component.scss' ]
} )
export class InfoComponent implements OnInit, OnDestroy {

  @Output () loginComponentEvent: EventEmitter<any>;

  private loginSubscribe: EventEmitter<any>;

  constructor ( private _es: EventsService ) {
    // Global Events
    this.loginSubscribe = this._es.getLoginEventEmitter ().subscribe ( ( event ) => this.loginEventListener ( event ) );

    // Local Events
    this.loginComponentEvent = new EventEmitter ();
  }

  public loginEventListener ( event ) {
    console.log ('InfoComponent - loginEventListener', event );
    this.loginComponentEvent.emit ( event );
  }

  ngOnInit (): void {
    console.log ( 'Hello Info' );
  }

  ngOnDestroy (): void {
    this.loginSubscribe.unsubscribe ();
  }
}
