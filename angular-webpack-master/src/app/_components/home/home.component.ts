import { Component, OnInit, EventEmitter, OnDestroy } from '@angular/core';
import { EventsService } from '../../_services/events.service';
import { CUser, IUser, UserService } from '../../_services/auth.service';
import { Subscription } from 'rxjs/Subscription';

@Component ( {
  selector: 'my-home',
  templateUrl: './home.component.html',
  styleUrls: [ './home.component.scss' ]
} )
export class HomeComponent implements OnInit, OnDestroy {

  private logoutSubscribe: EventEmitter<any>;
  private loginSubscribe: EventEmitter<any>;

  public user: CUser | IUser;
  public userRedux: CUser;

  private loginNGRXSubscription: Subscription;

  constructor ( private _es: EventsService,
                private _user: UserService ) {
    this.logoutSubscribe = this._es.getLogoutEventEmitter ().subscribe ( ( event ) => this.logoutEventListener ( event ) );
    this.loginSubscribe = this._es.getLoginEventEmitter ().subscribe ( ( event ) => this.loginEventListener ( event ) );

    this.loginNGRXSubscription = this._user.getStore('User').subscribe ( ( action: CUser ) => this.loginNGRXSubscriptionListener ( action ) );
  }

  public loginNGRXSubscriptionListener ( action: CUser ) {
    console.log ( action );
    this.userRedux = action;
    this.user = action;
  }

  public logoutEventListener ( event ) {
    console.log ( 'HomeComponent - logoutEventListener', event );
    this.user = null;
    this.userRedux = null;
  }

  public loginEventListener ( event ) {
    console.log ( 'HomeComponent - loginEventListener', event );
    this.user = event;
  }

  public loginSubComponentEvent ( event ) {
    console.log ( 'HomeComponent - loginSubComponentEvent', event );
  }

  public doLogin_1 () {
    this._user.doLoginPromise ( 'https://demo0215487.mockable.io/auth', { 'user': 'omar', 'password': '123456' } )
    .then ( ( response ) => {
      let cUser = new CUser ().fill ( response );
      console.log ( 'Promise - Onfulfilled (doLogin_1)', response );
      console.log ( 'Using CUser class', cUser );
      console.log ( cUser.getAllName () );
      this._es.loginEmitEvent ( cUser );
    } )
    .catch ( (error => {
      console.log ( error );
    }) );
  }

  public doLogin_2 () {
    let obs = this._user.doLoginObservable ( 'https://demo0215487.mockable.io/auth', { 'user': 'omar', 'password': '123456' } );
    obs.map ( ( response ) => {
      console.log ( 'Observable - Map (doLogin_2)', response );
      return response;
    } )
    .subscribe (
      ( response ) => {
        let iUser = <IUser> response;
        console.log ( 'Observable - Next (doLogin_2)', response );
        console.log ( 'Using IUser interface', iUser );
        this._es.loginEmitEvent ( iUser );
      },
      ( error ) => {
        console.log ( error );
      }, () => {

      } );
  }

  public doLogin_3 () {
    this._user.Worker ( 'https://demo0215487.mockable.io/auth', './workers/WDemo.js', { 'user': 'omar', 'password': '123456' } )
    .then ( ( json ) => {
      let iUser = <IUser> json;
      console.log ( 'Worker - Onfulfilled (doLogin_3)', json );
      console.log ( 'Using IUser interface', iUser );
      this._es.loginEmitEvent ( iUser );
    }, ( error ) => {
      console.error ( error );
    } );
  }

  public doLogin_4 () {
    this._es.doLoginEmitEvent();
  }

  public doLogout () {
    this._es.logoutEmitEvent ( true );
  }

  ngOnInit () {
    console.log ( 'Hello Home' );
    if ( this._user.isLoggued () ) {
      this.user = this._user.getUser ();
    }
  }

  ngOnDestroy (): void {
    this.logoutSubscribe.unsubscribe ();
    this.loginSubscribe.unsubscribe ();
    this.loginNGRXSubscription.unsubscribe();
  }
}
