"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var home_component_1 = require("./home.component");
var home_routing_1 = require("./home.routing");
var auth_service_1 = require("../../_services/auth.service");
var HomeModule = (function () {
    function HomeModule() {
    }
    return HomeModule;
}());
HomeModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            home_routing_1.HomeRoutes
        ],
        declarations: [
            home_component_1.HomeComponent
        ],
        exports: [
            home_component_1.HomeComponent
        ],
        providers: [
            auth_service_1.UserService
        ]
    })
], HomeModule);
exports.HomeModule = HomeModule;
//# sourceMappingURL=home.module.js.map