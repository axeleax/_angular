export const User: Reducer<any> = ( state: any = null, action: Action ) => {
  switch ( action.type ) {
    case 'GET_USER':
      return action.payload;
    default:
      return state;
  }
};
