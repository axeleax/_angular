"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var events_service_1 = require("../../_services/events.service");
var auth_service_1 = require("../../_services/auth.service");
var HomeComponent = (function () {
    function HomeComponent(_es, _user) {
        var _this = this;
        this._es = _es;
        this._user = _user;
        this.logoutSubscribe = this._es.getLogoutEventEmitter().subscribe(function (event) { return _this.logoutEventListener(event); });
        this.loginSubscribe = this._es.getLoginEventEmitter().subscribe(function (event) { return _this.loginEventListener(event); });
    }
    HomeComponent.prototype.logoutEventListener = function (event) {
        console.log(event);
    };
    HomeComponent.prototype.loginEventListener = function (event) {
        console.log(event);
        this.user = event;
    };
    HomeComponent.prototype.doLogin_1 = function () {
        var _this = this;
        this._user.doLogin('https://demo0215487.mockable.io/auth', { 'user': 'omar', 'password': '123456' })
            .subscribe(function (response) {
            var cUser = new auth_service_1.CUser().fill(response);
            _this._es.loginEmitEvent(cUser);
            console.log(cUser.getAllName());
        }, function (error) {
            console.log(error);
        });
    };
    HomeComponent.prototype.doLogin_2 = function () {
        var _this = this;
        var obs = this._user.doLogin('https://demo0215487.mockable.io/auth', { 'user': 'omar', 'password': '123456' });
        obs.map(function (response) {
            console.log(response);
            return response;
        })
            .subscribe(function (response) {
            var iUser = response;
            _this._es.loginEmitEvent(iUser);
            console.log(iUser);
        }, function (error) {
            console.log(error);
        }, function () {
        });
    };
    HomeComponent.prototype.doLogin_3 = function () {
        this._user.Worker('https://demo0215487.mockable.io/auth', './workers/WDemo.js', { 'user': 'omar', 'password': '123456' })
            .then(function (json) {
            console.log(json);
        }, function (error) {
            console.error(error);
        });
    };
    HomeComponent.prototype.ngOnInit = function () {
        console.log('Hello Home');
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    core_1.Component({
        selector: 'my-home',
        templateUrl: './home.component.html',
        styleUrls: ['./home.component.scss']
    }),
    __metadata("design:paramtypes", [events_service_1.EventsService, auth_service_1.UserService])
], HomeComponent);
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map