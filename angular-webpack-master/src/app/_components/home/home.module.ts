import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home.component';
import { HomeRoutes } from './home.routing';
import { UserService } from '../../_services/auth.service';
import { InfoModule } from './modules/info/info.module';

@NgModule ( {
  imports: [
    CommonModule,
    FormsModule,
    InfoModule,
    HomeRoutes
  ],
  declarations: [
    HomeComponent
  ],
  exports: [
    HomeComponent
  ],
  providers: [
    UserService
  ]
} )
export class HomeModule {
}
