"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var events_service_1 = require("../../_services/events.service");
var AboutComponent = (function () {
    function AboutComponent(_es) {
        var _this = this;
        this._es = _es;
        this.logoutSubscribe = this._es.getLogoutEventEmitter().subscribe(function (event) { return _this.logoutEventListener(event); });
        this.loginSubscribe = this._es.getLoginEventEmitter().subscribe(function (event) { return _this.loginEventListener(event); });
    }
    AboutComponent.prototype.logoutEventListener = function (event) {
        console.log(event);
    };
    AboutComponent.prototype.loginEventListener = function (event) {
        console.log(event);
    };
    AboutComponent.prototype.ngOnInit = function () {
        console.log('Hello About');
    };
    return AboutComponent;
}());
AboutComponent = __decorate([
    core_1.Component({
        selector: 'my-about',
        templateUrl: './about.component.html',
        styleUrls: ['./about.component.scss']
    }),
    __metadata("design:paramtypes", [events_service_1.EventsService])
], AboutComponent);
exports.AboutComponent = AboutComponent;
//# sourceMappingURL=about.component.js.map