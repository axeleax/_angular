import { RouterModule, Routes } from '@angular/router';
import { GuardService } from '../../_services/guard.service';
import { AboutComponent } from './about.component';

const routes: Routes = [
  { path: '', component: AboutComponent, canActivate: [ GuardService ] }
  // { path: 'about', component: AboutComponent, canActivate: [ GuardService ] }
];
export const AboutRoutes = RouterModule.forChild ( routes );
