import { Component, OnInit, OnDestroy } from '@angular/core';

@Component ( {
  selector: 'my-about',
  templateUrl: './about.component.html',
  styleUrls: [ './about.component.scss' ]
} )
export class AboutComponent implements OnInit, OnDestroy {

  constructor () {
  }

  ngOnInit (): void {
    console.log ( 'Hello About' );
  }

  ngOnDestroy (): void {
  }
}
