"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var about_component_1 = require("./about.component");
var about_routing_1 = require("./about.routing");
var AboutModule = (function () {
    function AboutModule() {
    }
    return AboutModule;
}());
AboutModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            about_routing_1.AboutRoutes
        ],
        declarations: [
            about_component_1.AboutComponent
        ],
        exports: [
            about_component_1.AboutComponent
        ],
        providers: []
    })
], AboutModule);
exports.AboutModule = AboutModule;
//# sourceMappingURL=about.module.js.map