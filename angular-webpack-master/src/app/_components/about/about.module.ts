import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AboutComponent } from './about.component';
import { AboutRoutes } from './about.routing';

@NgModule ( {
  imports: [
    CommonModule,
    FormsModule,
    AboutRoutes
  ],
  declarations: [
    AboutComponent
  ],
  exports: [
    AboutComponent
  ],
  providers: []
} )
export class AboutModule {
}
