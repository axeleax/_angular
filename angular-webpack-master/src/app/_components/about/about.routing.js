"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var guard_service_1 = require("../../_services/guard.service");
var about_component_1 = require("./about.component");
var routes = [
    { path: '', component: about_component_1.AboutComponent, canActivate: [guard_service_1.GuardService] }
    // { path: 'about', component: AboutComponent, canActivate: [ GuardService ] }
];
exports.AboutRoutes = router_1.RouterModule.forChild(routes);
//# sourceMappingURL=about.routing.js.map