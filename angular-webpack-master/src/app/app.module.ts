import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutes } from './app.routing';

import { EventsService } from './_services/events.service';
// import { HomeModule } from './_components/home/home.module';
// import { AboutModule } from './_components/about/about.module';
import { GuardService } from './_services/guard.service';
import { UserService } from './_services/auth.service';
import { HomeProvider } from './_components/home/home.provider';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

@NgModule ( {
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    AppRoutes,
    HomeProvider,
    StoreDevtoolsModule.instrumentOnlyWithExtension()
    // HomeModule,
    // AboutModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    EventsService,
    GuardService,
    UserService
  ],
  exports: [RouterModule],
  bootstrap: [ AppComponent ]
} )
export class AppModule {
}
