"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
// import { provideRoutes } from '@angular/router';
var testing_2 = require("@angular/router/testing");
// import { ApiService } from './shared';
var app_component_1 = require("./app.component");
describe('App', function () {
    // provide our implementations or mocks to the dependency injector
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            imports: [testing_2.RouterTestingModule],
            declarations: [app_component_1.AppComponent],
        });
    });
    it('should have an url', function () {
        var fixture = testing_1.TestBed.createComponent(app_component_1.AppComponent);
        fixture.detectChanges();
        expect(fixture.debugElement.componentInstance.url).toEqual('https://github.com/preboot/angular2-webpack');
    });
});
//# sourceMappingURL=app.component.spec.js.map