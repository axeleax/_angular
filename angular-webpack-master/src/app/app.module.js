"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require("@angular/router");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var app_component_1 = require("./app.component");
var app_routing_1 = require("./app.routing");
var events_service_1 = require("./_services/events.service");
// import { HomeModule } from './_components/home/home.module';
// import { AboutModule } from './_components/about/about.module';
var guard_service_1 = require("./_services/guard.service");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            http_1.HttpModule,
            forms_1.FormsModule,
            app_routing_1.AppRoutes,
        ],
        declarations: [
            app_component_1.AppComponent
        ],
        providers: [
            events_service_1.EventsService,
            guard_service_1.GuardService
        ],
        exports: [router_1.RouterModule],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map