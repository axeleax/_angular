"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var routes = [
    { path: '', loadChildren: './_components/home/home.module#HomeModule' },
    { path: 'about', loadChildren: './_components/about/about.module#AboutModule' }
];
exports.AppRoutes = router_1.RouterModule.forRoot(routes);
//# sourceMappingURL=app.routing.js.map