import { Component, EventEmitter, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import '../style/app.scss';
import { EventsService } from './_services/events.service';
import { CUser, IUser, UserService } from './_services/auth.service';
import { Subscription } from 'rxjs/Subscription';

// import { importExpr } from '@angular/compiler/src/output/output_ast';

@Component ( {
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ],
  encapsulation: ViewEncapsulation.None
} )
export class AppComponent implements OnInit, OnDestroy {

  private logoutSubscribe: EventEmitter<any>;
  private loginSubscribe: EventEmitter<any>;

  public url = 'https://github.com/preboot/angular2-webpack';
  public title: string;

  public user: CUser | IUser;

  public userRedux: CUser;

  private loginNGRXSubscription: Subscription;

  constructor ( private _es: EventsService,
                private _user: UserService) {

    this.title = 'Example';

    // Global Events
    this.logoutSubscribe = this._es.getLogoutEventEmitter ().subscribe ( ( event ) => this.logoutEventListener ( event ) );
    this.loginSubscribe = this._es.getLoginEventEmitter ().subscribe ( ( event ) => this.loginEventListener ( event ) );

    this.loginNGRXSubscription = this._user.getStore('User').subscribe ( ( action: CUser ) => this.loginNGRXSubscriptionListener ( action ) );
  }

  public loginNGRXSubscriptionListener ( action: CUser ) {
    console.log ( action );
    this.userRedux = action;
    this.user = action;
    this._user.doCreateLocalStorage ( this.user );
  }

  public logoutEventListener ( event ) {
    console.log ( 'AppComponent - logoutEventListener', event );
    this._user.doLogout ();
    this.user = this._user.getUser ();
    this.userRedux = this._user.getUser ();
  }

  public loginEventListener ( event ) {
    console.log ( 'AppComponent - loginEventListener', event );
    this.user = event;
    this._user.doCreateLocalStorage ( this.user );
  }

  ngOnInit (): void {
    console.log ( 'Hello App' );
  }

  ngOnDestroy (): void {
    this.logoutSubscribe.unsubscribe ();
    this.loginSubscribe.unsubscribe ();
    this.loginNGRXSubscription.unsubscribe();
  }
}
