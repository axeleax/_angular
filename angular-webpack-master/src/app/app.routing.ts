import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
   { path: '', loadChildren: './_components/home/home.module#HomeModule' },
   { path: 'about', loadChildren: './_components/about/about.module#AboutModule'}
];

export const AppRoutes = RouterModule.forRoot ( routes );

