"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
require("../style/app.scss");
var events_service_1 = require("./_services/events.service");
var AppComponent = (function () {
    function AppComponent(_es) {
        var _this = this;
        this._es = _es;
        this.url = 'https://github.com/preboot/angular2-webpack';
        this.title = 'Example';
        this.loginSubscribe = this._es.getLoginEventEmitter().subscribe(function (event) { return _this.loginEventListener(event); });
    }
    AppComponent.prototype.loginEventListener = function (event) {
        console.log(event);
        this.user = event;
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'my-app',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.scss'],
        encapsulation: core_1.ViewEncapsulation.None
    }),
    __metadata("design:paramtypes", [events_service_1.EventsService])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map