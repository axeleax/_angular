"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var EventsService = (function () {
    function EventsService() {
        this.logoutEventEmitter = new core_1.EventEmitter();
        this.loginEventEmitter = new core_1.EventEmitter();
    }
    EventsService.prototype.getLogoutEventEmitter = function () {
        return this.logoutEventEmitter;
    };
    EventsService.prototype.logoutEmitEvent = function (any) {
        this.logoutEventEmitter.emit(any);
    };
    EventsService.prototype.getLoginEventEmitter = function () {
        return this.loginEventEmitter;
    };
    EventsService.prototype.loginEmitEvent = function (any) {
        this.loginEventEmitter.emit(any);
    };
    return EventsService;
}());
EventsService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], EventsService);
exports.EventsService = EventsService;
//# sourceMappingURL=events.service.js.map