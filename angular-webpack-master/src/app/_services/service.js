"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("@angular/http");
var Rx_1 = require("rxjs/Rx");
var METHOD;
(function (METHOD) {
    METHOD[METHOD["GET"] = 0] = "GET";
    METHOD[METHOD["POST"] = 1] = "POST";
    METHOD[METHOD["PUT"] = 2] = "PUT";
    METHOD[METHOD["DELETE"] = 3] = "DELETE";
    METHOD[METHOD["PATCH"] = 4] = "PATCH";
    METHOD[METHOD["REQUEST"] = 5] = "REQUEST";
    METHOD[METHOD["HEAD"] = 6] = "HEAD";
    METHOD[METHOD["OPTIONS"] = 7] = "OPTIONS";
})(METHOD = exports.METHOD || (exports.METHOD = {}));
var TYPE;
(function (TYPE) {
    TYPE[TYPE["PROMISE"] = 0] = "PROMISE";
    TYPE[TYPE["OBSERVABLE"] = 1] = "OBSERVABLE";
})(TYPE = exports.TYPE || (exports.TYPE = {}));
var RESPONSE_STATUS;
(function (RESPONSE_STATUS) {
    RESPONSE_STATUS[RESPONSE_STATUS["SUCCESS_200"] = 200] = "SUCCESS_200";
    RESPONSE_STATUS[RESPONSE_STATUS["SUCCESS_201"] = 201] = "SUCCESS_201";
    RESPONSE_STATUS[RESPONSE_STATUS["ERROR_401"] = 401] = "ERROR_401";
    RESPONSE_STATUS[RESPONSE_STATUS["ERROR_404"] = 404] = "ERROR_404";
    RESPONSE_STATUS[RESPONSE_STATUS["ERROR_500"] = 500] = "ERROR_500";
})(RESPONSE_STATUS = exports.RESPONSE_STATUS || (exports.RESPONSE_STATUS = {}));
exports.CONTENT_TYPE = {
    JSON: 'application/json; charset=UTF-8',
    FORM: 'multipart/form-data'
};
var SCService = (function () {
    function SCService(_http, _es) {
        this._http = _http;
        this._es = _es;
        this._contentType = exports.CONTENT_TYPE.JSON;
    }
    // For global events
    SCService.prototype.logoutEmitEvent = function (data) {
        this._es.logoutEventEmitter.emit(data);
    };
    SCService.prototype.getHeaders = function () {
        var headers = new http_1.Headers();
        headers.append('Content-Type', this._contentType);
        return headers;
    };
    SCService.prototype.getHeadersMockable = function () {
        var headers = new http_1.Headers();
        headers.append('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
        headers.append('Access-Control-Origin', '*');
        headers.append('Access-Control-Max-Age', '3600');
        headers.append('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Access-Control-origin');
        return headers;
    };
    SCService.prototype.getRequestOptions = function (mockable) {
        var requestOptions = new http_1.RequestOptions();
        requestOptions.withCredentials = !mockable;
        requestOptions.headers = (!mockable) ? this.getHeaders() : this.getHeadersMockable();
        return requestOptions;
    };
    SCService.prototype.Http = function (method, url, data, options, type, directError, mockable) {
        var _this = this;
        var requestOptions = (options) ? options : this.getRequestOptions(mockable);
        var self = this;
        switch (method) {
            case METHOD.GET:
            case METHOD.DELETE:
            case METHOD.REQUEST:
            case METHOD.PATCH:
            case METHOD.HEAD:
            case METHOD.OPTIONS:
                if (TYPE.OBSERVABLE === type) {
                    return this._http[METHOD[method].toLocaleLowerCase()](url, requestOptions)
                        .map(this.extractData)
                        .catch(function (error) { return _this.handleErrorObservable(error, self, directError); });
                }
                else {
                    return this._http[METHOD[method].toLocaleLowerCase()](url, requestOptions)
                        .toPromise()
                        .then(this.extractData)
                        .catch(function (error) { return _this.handlePromise(error, self, directError); });
                }
            case METHOD.POST:
            case METHOD.PUT:
                if (TYPE.OBSERVABLE === type) {
                    return this._http[METHOD[method].toLocaleLowerCase()](url, data, requestOptions)
                        .map(this.extractData)
                        .catch(function (error) { return _this.handleErrorObservable(error, self, directError); });
                }
                else {
                    return this._http[METHOD[method].toLocaleLowerCase()](url, data, requestOptions)
                        .toPromise()
                        .then(this.extractData)
                        .catch(function (error) { return _this.handlePromise(error, self, directError); });
                }
        }
    };
    ;
    SCService.prototype.Worker = function (serviceURL, workerURL, json) {
        return new Promise(function (resolve, reject) {
            var worker = new Worker(workerURL);
            worker.addEventListener('message', function (e) {
                if (e.data['status'] === 200) {
                    console.info('Worker return with correctly:');
                    resolve(e['data'].user);
                }
                else {
                    console.info('Worker return with errors:');
                    reject(e['error']);
                }
            });
            worker.postMessage({ url: serviceURL, json: json });
        });
    };
    SCService.prototype.extractData = function (response) {
        var body = (response.status >= 200 && response.status < 300);
        try {
            body = response.json();
        }
        catch (e) {
            body = response;
        }
        return body || null;
    };
    SCService.prototype.handleErrorObservable = function (error, self, directError) {
        error = error.json();
        if (!directError) {
            if (RESPONSE_STATUS.ERROR_401 === error.status) {
                self.logoutEndSessionEmitEvent();
                return false;
            }
            return Rx_1.Observable.throw({
                'status': error.status,
                'message': (error.message) ? error.message : error.status ? error.status + " - " + error.statusText : 'Server error',
                'error': error
            });
        }
        else {
            return Rx_1.Observable.throw({
                'status': error.status,
                'message': (error.message) ? error.message : error.status ? error.status + " - " + error.statusText : 'Server error',
                'error': error
            });
        }
    };
    SCService.prototype.handlePromise = function (error, self, directError) {
        error = error.json();
        if (!directError) {
            if (RESPONSE_STATUS.ERROR_401 === error.status) {
                self.logoutEmitEvent();
                return false;
            }
            return Promise.reject({
                'status': error.status,
                'message': (error.message) ? error.message : error.status ? error.status + " - " + error.statusText : 'Server error',
                'error': error
            });
        }
        else {
            return Rx_1.Observable.throw({
                'status': error.status,
                'message': (error.message) ? error.message : error.status ? error.status + " - " + error.statusText : 'Server error',
                'error': error
            });
        }
    };
    return SCService;
}());
exports.SCService = SCService;
//# sourceMappingURL=service.js.map