"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var service_1 = require("./service");
var events_service_1 = require("./events.service");
var UserService = (function (_super) {
    __extends(UserService, _super);
    function UserService(_http, _es) {
        var _this = _super.call(this, _http, _es) || this;
        _this._http = _http;
        _this._es = _es;
        return _this;
    }
    UserService.prototype.doLogin = function (url, data) {
        return this.Http(service_1.METHOD.POST, url, data, null, service_1.TYPE.OBSERVABLE, false, true);
    };
    return UserService;
}(service_1.SCService));
UserService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, events_service_1.EventsService])
], UserService);
exports.UserService = UserService;
// Entities
var CUser = (function () {
    function CUser() {
    }
    CUser.prototype.fill = function (_json) {
        if (_json) {
            Object.assign(this, _json);
        }
        return this;
    };
    CUser.prototype.getAllName = function () {
        return this.first_name + ' ' + this.last_name;
    };
    return CUser;
}());
exports.CUser = CUser;
//# sourceMappingURL=auth.service.js.map