import { Response, Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { EventsService } from './events.service';
import { Store } from '@ngrx/store';

export enum METHOD {
  'GET',
  'POST',
  'PUT',
  'DELETE',
  'PATCH',
  'REQUEST',
  'HEAD',
  'OPTIONS'
}

export enum TYPE {
  'PROMISE',
  'OBSERVABLE'
}

export enum RESPONSE_STATUS {
  'SUCCESS_200' = 200,
  'SUCCESS_201' = 201,
  'ERROR_401' = 401,
  'ERROR_404' = 404,
  'ERROR_500' = 500
}

export const CONTENT_TYPE = {
  JSON: 'application/json; charset=UTF-8',
  FORM: 'multipart/form-data'
};

export class SCService {

  public _contentType: string;

  constructor ( public _http: Http,
                public _es: EventsService,
                public _store ?: Store<any> ) {
    this._contentType = CONTENT_TYPE.JSON;
  }

  // For global events
  public logoutEmitEvent ( data?: any ): void {
    this._es.logoutEventEmitter.emit ( data );
  }

  private getHeaders (): Headers {
    let headers = new Headers ();
    headers.append ( 'Content-Type', this._contentType );
    return headers;
  }

  private getHeadersMockable (): Headers {
    let headers = new Headers ();
    headers.append ( 'Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT' );
    headers.append ( 'Access-Control-Origin', '*' );
    headers.append ( 'Access-Control-Max-Age', '3600' );
    headers.append ( 'Access-Control-Allow-Headers',
      'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Access-Control-origin' );
    return headers;
  }

  public getRequestOptions ( mockable?: boolean ): RequestOptions {
    let requestOptions = new RequestOptions ();
    requestOptions.withCredentials = !mockable;
    requestOptions.headers = (!mockable) ? this.getHeaders () : this.getHeadersMockable ();
    return requestOptions;
  }

  public Http ( method: METHOD, url: string, data: any, options: RequestOptions, type: TYPE, directError?: boolean, mockable?: boolean ): any {
    let requestOptions = (options) ? options : this.getRequestOptions ( mockable );
    let self = this;
    switch ( method ) {
      case METHOD.GET :
      case METHOD.DELETE :
      case METHOD.REQUEST :
      case METHOD.PATCH :
      case METHOD.HEAD :
      case METHOD.OPTIONS :
        if ( TYPE.OBSERVABLE === type ) {
          return this._http[ METHOD[ method ].toLocaleLowerCase () ] ( url, requestOptions )
          .map ( this.extractData )
          .catch ( ( error ) => this.handleErrorObservable ( error, self, directError ) );
        } else {
          return this._http[ METHOD[ method ].toLocaleLowerCase () ] ( url, requestOptions )
          .toPromise ()
          .then ( this.extractData )
          .catch ( ( error ) => this.handlePromise ( error, self, directError ) );
        }
      case METHOD.POST :
      case METHOD.PUT :
        if ( TYPE.OBSERVABLE === type ) {
          return this._http[ METHOD[ method ].toLocaleLowerCase () ] ( url, data, requestOptions )
          .map ( this.extractData )
          .catch ( ( error ) => this.handleErrorObservable ( error, self, directError ) );
        } else {
          return this._http[ METHOD[ method ].toLocaleLowerCase () ] ( url, data, requestOptions )
          .toPromise ()
          .then ( this.extractData )
          .catch ( ( error ) => this.handlePromise ( error, self, directError ) );
        }
    }
  };

  public Worker ( serviceURL: string, workerURL: string, json?: any ): Promise<any> {
    return new Promise ( ( resolve, reject ) => {
      let worker = new Worker ( workerURL );
      worker.addEventListener ( 'message', ( e: MessageEvent ) => {
        if ( e.data[ 'status' ] === 200 ) {
          console.info ( 'Worker return with correctly:' );
          resolve ( e[ 'data' ].user );
        } else {
          console.info ( 'Worker return with errors:' );
          reject ( e[ 'error' ] );
        }
      } );
      worker.postMessage ( { url: serviceURL, json: json } );
    } );
  }

  public extractData ( response: Response ) {
    let body: any = (response.status >= 200 && response.status < 300);
    try {
      body = response.json ();
    } catch ( e ) {
      body = response;
    }
    return body || null;
  }

  public handleErrorObservable ( error: any, self: any, directError?: boolean ): any {
    error = error.json ();
    if ( !directError ) {
      if ( RESPONSE_STATUS.ERROR_401 === error.status ) {
        self.logoutEndSessionEmitEvent ();
        return false;
      }
      return Observable.throw ( {
        'status': error.status,
        'message': (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error',
        'error': error
      } );
    } else {
      return Observable.throw ( {
        'status': error.status,
        'message': (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error',
        'error': error
      } );
    }
  }

  public handlePromise ( error: any, self: any, directError?: boolean ): any {
    error = error.json ();
    if ( !directError ) {
      if ( RESPONSE_STATUS.ERROR_401 === error.status ) {
        self.logoutEmitEvent ();
        return false;
      }
      return Promise.reject ( {
        'status': error.status,
        'message': (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error',
        'error': error
      } );
    } else {
      return Observable.throw ( {
        'status': error.status,
        'message': (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error',
        'error': error
      } );
    }
  }

  public getStore ( store: string ) {
    return this._store.select ( store );
  }

  public dispatchStore ( action: Action ) {
    this._store.dispatch ( action );
  }
}

