import { EventEmitter, Injectable } from '@angular/core';

@Injectable ()
export class EventsService {

  // Global events output
  public logoutEventEmitter: EventEmitter<any>;
  public loginEventEmitter: EventEmitter<any>;

  // Global events input
  public dologinEventEmitter: EventEmitter<any>;

  constructor () {
    this.logoutEventEmitter = new EventEmitter<any> ();
    this.loginEventEmitter = new EventEmitter<any> ();

    this.dologinEventEmitter = new EventEmitter<any> ();
  }

  public getLogoutEventEmitter (): EventEmitter<any> {
    return this.logoutEventEmitter;
  }

  public logoutEmitEvent ( any ): void {
    this.logoutEventEmitter.emit ( any );
  }

  public getLoginEventEmitter (): EventEmitter<any> {
    return this.loginEventEmitter;
  }

  public loginEmitEvent ( any ): void {
    this.loginEventEmitter.emit ( any );
  }

  public getDoLoginEventEmitter (): EventEmitter<any> {
    return this.dologinEventEmitter;
  }

  public doLoginEmitEvent ( ): void {
    this.dologinEventEmitter.emit ( );
  }

}
