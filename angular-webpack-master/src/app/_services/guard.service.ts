import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, CanActivateChild, Route, ActivatedRouteSnapshot, RouterStateSnapshot/*, Router*/ } from '@angular/router';
import { UserService } from './auth.service';

@Injectable ()
export class GuardService implements CanActivate, CanActivateChild, CanLoad {

  constructor ( /*private _router: Router,*/
                private _user: UserService ) {
  }

  canActivate ( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): boolean {
    let url: string = state.url;
    return this.checkLogin ( route, url );
  }

  canActivateChild ( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): boolean {
    return this.canActivate ( route, state );
  }

  canLoad ( route: Route ): boolean {
    let url = `/${route.path}`;
    return this.checkLogin ( route, url );
  }

  checkLogin ( route: any, url: string ): boolean {
    console.log ( route + '- >' + url );
    // Review if is logged via service
    return this._user.isLoggued ();
  }
}
