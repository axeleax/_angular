import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import { METHOD, TYPE, SCService } from './service';
import { EventsService } from './events.service';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

@Injectable ()
export class UserService extends SCService {

  private doLoginSubscribe: EventEmitter<any>;

  constructor ( public _http: Http, public _es: EventsService, _store: Store<any> ) {
    super ( _http, _es, _store );

    this.doLoginSubscribe = this._es.getDoLoginEventEmitter ().subscribe ( ( event ) => this.doLoginEventListener ( event ) );
  }

  public doLoginEventListener ( event ) {
    console.log ( 'UserService - doLoginEventListener', event );
    this.doLoginRedux();
  }

  public doLoginPromise ( url: string, data: any ): Promise<any> {
    return this.Http ( METHOD.POST, url, data, null, TYPE.PROMISE, false, true );
  }

  public doLoginObservable ( url: string, data: any ): Observable<any> {
    return this.Http ( METHOD.POST, url, data, null, TYPE.OBSERVABLE, false, true );
  }

  public doLoginRedux () {
    this.doLoginObservable ( 'https://demo0215487.mockable.io/auth', { 'user': 'omar', 'password': '123456' } )
    .subscribe (
      ( payload ) => {
        this.dispatchStore ( { type: 'GET_USER', payload } );
      },
      ( error ) => {
        console.log ( error );
      } );
  }

  public doLogout () {
    console.log ( 'Logout' );
    if ( localStorage.getItem ( 'USER' ) ) {
      localStorage.removeItem ( 'USER' );
      localStorage.removeItem ( 'FIRST_NAME' );
      localStorage.removeItem ( 'LAST_NAME' );
      console.log ( 'LocalStorage.removeItem ->USER' );
    }
  }

  public doCreateLocalStorage ( user: CUser | IUser ): boolean {
    console.log ( 'CreateLocalStorage' );
    let valid = !!(user && user.user);
    if ( valid ) {
      localStorage.setItem ( 'USER', user.user );
      localStorage.setItem ( 'FIRST_NAME', user.first_name );
      localStorage.setItem ( 'LAST_NAME', user.last_name );
      console.log ( 'LocalStorage.setItem -> USER (' + JSON.stringify ( user ) + ')' );
    }
    return valid;
  }

  public isLoggued (): boolean {
    return ( localStorage.getItem ( 'USER' ) !== null);
  }

  public getUser (): CUser {
    let user = null;
    if ( localStorage.getItem ( 'USER' ) ) {
      user = new CUser ().fill ( {
        'user': localStorage.getItem ( 'USER' ),
        'first_name': localStorage.getItem ( 'FIRST_NAME' ),
        'last_name': localStorage.getItem ( 'LAST_NAME' )
      } );
    }
    return user;
  }
}

// Entities
export class CUser {

  public user: string;
  public first_name: string;
  public last_name: string;

  constructor () {
  }

  public fill ( _json: any ): any {
    if ( _json ) {
      Object.assign ( this, _json );
    }
    return this;
  }

  public getAllName (): string {
    return this.first_name + ' ' + this.last_name;
  }
}

export interface IUser {
  user: string;
  first_name: string;
  last_name: string;
}
