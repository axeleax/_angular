"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var GuardService = (function () {
    function GuardService() {
    }
    GuardService.prototype.canActivate = function (route, state) {
        var url = state.url;
        return this.checkLogin(route, url);
    };
    GuardService.prototype.canActivateChild = function (route, state) {
        return this.canActivate(route, state);
    };
    GuardService.prototype.canLoad = function (route) {
        var url = "/" + route.path;
        return this.checkLogin(route, url);
    };
    GuardService.prototype.checkLogin = function (route, url) {
        console.log(route.url + '- >' + url);
        // Session
        /*let session: Session = new Session ().fill ( this._gc.localStorage.decodeBase64 ( 'SESSION_PORTAL' ) );
         if ( session.isAuthenticated () ) {
         this._authService.isLoginEvent = false;
         this._authService.loadSession ( session ).then ( () => {
         console.info ( 'SESSION was loaded.', this._gc.localStorage.decodeBase64 ( 'SESSION_PORTAL' ) );
         this._authService.loadedSessionEmitEvent ( { 'url': url } );
         } ).catch ( () => {
         console.warn ( 'Unknown SESSION' );
         } );
         }
         if ( new Session ().fill ( this._authService.session ).isAuthenticated () ) {
         return true;
         }
         // Store the attempted URL for redirecting
         this._authService.redirectUrl = url;
    
         // Navigate to the login page with extras
         this._router.navigate ( [ '' ] );*/
        return true;
    };
    return GuardService;
}());
GuardService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], GuardService);
exports.GuardService = GuardService;
//# sourceMappingURL=guard.service.js.map